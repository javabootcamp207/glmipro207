package com.techno207Proj.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.techno207Proj.model.UsersModel;

public interface UsersRepository extends JpaRepository<UsersModel,Integer>{

}
