package com.techno207Proj.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.techno207Proj.model.UsersModel;

public interface LoginRepository extends JpaRepository<UsersModel,Integer>{

	  @Query(value = "SELECT u.nama FROM UsersModel u where u.nama = ?1 and u.sandi= ?2 ")
	  String cekLogin(String nama,String pass);
	
}
