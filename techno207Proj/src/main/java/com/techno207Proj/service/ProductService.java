package com.techno207Proj.service;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.techno207Proj.model.ProductModel;
import com.techno207Proj.repository.ProductRepository;

@Service
@Transactional
public class ProductService {
	@Autowired
	private ProductRepository prdc;
	
	//default select product semua
	public List<ProductModel> listAll() {
		return prdc.findAll();
	}
	
	//ByQuery select product semua
		public List<ProductModel> ProdList() {
			return prdc.ProdList();
		}
	
	//ByQuery select product by . . .
	public List<ProductModel> ProdByName(String nama) {
		return prdc.ProdByName(nama);
	}
	
	public ProductModel save(ProductModel ProductModel) {
		return prdc.save(ProductModel);
	}
	
	public ProductModel get(int idproduct) {
		return prdc.findById(idproduct).get();
	}
	
	public void delete(int id) {
		prdc.deleteById(id);
	}
	
	
}
