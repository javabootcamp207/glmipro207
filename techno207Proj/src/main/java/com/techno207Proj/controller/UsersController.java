package com.techno207Proj.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.techno207Proj.model.UsersModel;

import com.techno207Proj.service.UsersService;

@Controller
public class UsersController {
	
	@Autowired
	private UsersService uss;
	
	@RequestMapping("/users")
	public String users(HttpSession session) {
		Object logses =  session.getAttribute("logsesUser");
		if (logses == null) {
			logses = new String();
		}
		String lg1 = logses.toString();
		
		String stsreturn="";
		if (lg1 != null && !lg1.isEmpty()) {
					stsreturn="Users/users";
				} else {
					stsreturn="redirect:/login";
				}
		return stsreturn;
	}
	
	@RequestMapping("/userlist")
	public String userlist(Model model) {
		List<UsersModel> userlist = uss.listAll();
		model.addAttribute("userlist", userlist);
		return "Users/userlist";
	}
	
	@RequestMapping("/usersnew")
	public String usersnew(Model model) {
		
		return "Users/usersnew";
	}
	
	// simpan dan edit data
	@ResponseBody
	@RequestMapping(value = "/saveusr/{sts}")
	public String saveusr(@ModelAttribute("UsersModelDepan") UsersModel UsersModel,
							HttpSession session,
							@PathVariable(name = "sts") String sts) {
		String savests = "";
		Object logses =  session.getAttribute("logsesUser");
		if (logses == null) {
			logses = new String();
		}
		
		//bagi antara baru dan simpan
		String lg1 = logses.toString();
		
		long millis=System.currentTimeMillis();  
		java.sql.Date date=new java.sql.Date(millis);  
		
		if (sts.equals("new")) {
			UsersModel.setCreateby(lg1);
			UsersModel.setCreatedate(date);
		} else {
			UsersModel.setModifby(lg1);
			UsersModel.setModifdate(date);
		}
		uss.save(UsersModel);
		return savests;
	}
	
	@RequestMapping("/usersdelfrm/{id}")
	public String deletefromsply(@PathVariable(name = "id") int id, Model model) {
		UsersModel usersmodel = uss.get(id);
		model.addAttribute("usersmodeldepan", usersmodel);
		return "Users/usersdelet";
	}
	
	@ResponseBody
	@RequestMapping("/deleteusr/{id}")
	public String deleteSply(@PathVariable(name = "id") int id) {
		String delsts = "0";
		
			uss.delete(id);
	
		return delsts;
	}
	
	@RequestMapping("/useredit/{id}")
	public String editfromsply(@PathVariable(name = "id") int id, Model model) {
		UsersModel usersmodel = uss.get(id);
		model.addAttribute("usersmodeldepan", usersmodel);
		
		
		return "Users/usersedit";
	}
}
