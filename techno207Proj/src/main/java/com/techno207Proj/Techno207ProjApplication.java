package com.techno207Proj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Techno207ProjApplication {

	public static void main(String[] args) {
		SpringApplication.run(Techno207ProjApplication.class, args);
	}

}
