package com.techno207Proj.model;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "supplier")
public class SupplierModel {
	@Id
	@Column(name = "id_supplier")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idsupplier;
	@Column(name = "nama")
	private String nama;
	@Column(name = "alamat")
	private String alamat;
	@Column(name = "email")
	private String email;
	public int getIdsupplier() {
		return idsupplier;
	}
	public void setIdsupplier(int idsupplier) {
		this.idsupplier = idsupplier;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
}
