package com.techno207Proj.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "users")
public class UsersModel {
	@Id
	@Column(name = "id_users")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int idusers;
	@Column(name = "nama", nullable = false)
	private String nama;
	@Column(name = "alamat", nullable = false)
	private String alamat;
	@Column(name = "umur")
	private int umur;
	@Column(name = "gender")
	private boolean gender;
	@Column(name = "sandi")
	private String sandi;
	@Column(name = "createby")
	private String createby;
	
	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Column(name = "createdate")
	private Date createdate;
	@Column(name = "modifby")
	private String modifby;
	@Column(name = "modifdate")
	private Date modifdate;
	public int getIdusers() {
		return idusers;
	}
	public void setIdusers(int idusers) {
		this.idusers = idusers;
	}
	public String getNama() {
		return nama;
	}
	public void setNama(String nama) {
		this.nama = nama;
	}
	public String getAlamat() {
		return alamat;
	}
	public void setAlamat(String alamat) {
		this.alamat = alamat;
	}
	public int getUmur() {
		return umur;
	}
	public void setUmur(int umur) {
		this.umur = umur;
	}
	public boolean isGender() {
		return gender;
	}
	public void setGender(boolean gender) {
		this.gender = gender;
	}
	public String getSandi() {
		return sandi;
	}
	public void setSandi(String sandi) {
		this.sandi = sandi;
	}
	public String getCreateby() {
		return createby;
	}
	public void setCreateby(String createby) {
		this.createby = createby;
	}
	public Date getCreatedate() {
		return createdate;
	}
	public void setCreatedate(Date createdate) {
		this.createdate = createdate;
	}
	public String getModifby() {
		return modifby;
	}
	public void setModifby(String modifby) {
		this.modifby = modifby;
	}
	public Date getModifdate() {
		return modifdate;
	}
	public void setModifdate(Date modifdate) {
		this.modifdate = modifdate;
	}
	
		
	
}
